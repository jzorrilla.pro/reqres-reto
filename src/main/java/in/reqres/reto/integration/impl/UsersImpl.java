package in.reqres.reto.integration.impl;

import in.reqres.reto.dto.UserClientResponse;
import in.reqres.reto.dto.UsersResponse;
import in.reqres.reto.exception.BaseException;
import in.reqres.reto.integration.UserService;
import in.reqres.reto.util.Constantes;
import in.reqres.reto.util.PropertiesExternos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.Collections;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UsersImpl implements UserService {
    Logger logger = Logger.getLogger(String.valueOf(UsersImpl.class));

    @Autowired
    private PropertiesExternos prop;

    @Override
    public UsersResponse getUsers(String transaction) throws BaseException {
        String url = prop.wsReqresUsersUrl;
        long initTime = System.currentTimeMillis();
        logger.log(Level.INFO, transaction + " - Start client REQREST-USERS");
        logger.log(Level.INFO, "URL: " + url);
        UsersResponse usersResponse = new UsersResponse();
        try{
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.add("user-agent", Constantes.USER_AGENT);
            HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

            ResponseEntity<UserClientResponse> objectResponseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,UserClientResponse.class);
            UserClientResponse clientResponse = objectResponseEntity.getBody();
            if(clientResponse!=null) usersResponse.setData(clientResponse.getData());

        }catch (Exception e){
            throw new BaseException(prop.idt1Codigo,e.getMessage());
        }finally {
            logger.log(Level.INFO, transaction +" - " + String.format(Constantes.TIME_TOTAL_CLIENT, (System.currentTimeMillis() - initTime)));
            logger.log(Level.INFO, transaction +" - End client REQREST-USERS");
        }

        return usersResponse;
    }
}
