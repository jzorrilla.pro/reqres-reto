package in.reqres.reto.integration;


import in.reqres.reto.dto.UsersResponse;
import in.reqres.reto.exception.BaseException;

public interface UserService {

       public UsersResponse getUsers(String transaction) throws BaseException;
}
