package in.reqres.reto.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesExternos {

    /* IDT */
    @Value("${idt1.codigo}")
    public String idt1Codigo;

    @Value("${idt1.mensaje}")
    public String idt1Mensaje;

    /* IDF */
    @Value("${idf0.codigo}")
    public String idf0Codigo;
    @Value("${idf0.mensaje}")
    public String idf0Mensaje;

    @Value("${ws.reqres.users.url}")
    public String wsReqresUsersUrl;
}
