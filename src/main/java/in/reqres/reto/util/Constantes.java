package in.reqres.reto.util;

public class Constantes {
    public static final String PROPERTIESKEY = "reqres-app.properties";
    public static final String NOMBRERECURSO = "reqres-reto";
    public static final String BASE_PATH = "/api";

    public static final String CHARSET = ";charset=UTF-8";
    public static final String APPLICATION_JSON = "application/json";
    public static final String TIME_TOTAL_PROCESS = "Time total process: [ %s milliseconds ]";
    public static final String TIME_TOTAL_CLIENT = "Time total client: [ %s milliseconds ]";

    //Method Users
    public static final String PATH_USERS = "/users";
    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";

}
