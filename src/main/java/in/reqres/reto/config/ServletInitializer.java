package in.reqres.reto.config;

import java.io.File;
import java.util.Properties;

import in.reqres.reto.RetoApplication;
import in.reqres.reto.util.Constantes;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RetoApplication.class).properties(getProperties());
    }

    static Properties getProperties() {
        Properties props = new Properties();
        StringBuilder build = new StringBuilder();
        /*
        build.append(System.getProperty("user.dir"));//.replace("\\", "/");
        build.append(File.separator);
        build.append(Constantes.PROPERTIESKEY);
        */
        /*
        build.append(Constantes.NOMBRERECURSO);
        build.append(File.separator);
        build.append("application.properties");
        */
        props.put("spring.config.location", "file:" + build);

        return props;
    }
}
