package in.reqres.reto.dto;

import java.util.List;

public class UsersResponse {
    private List<UserDto> data;

    public List<UserDto> getData() {
        return data;
    }

    public void setData(List<UserDto> data) {
        this.data = data;
    }
}
