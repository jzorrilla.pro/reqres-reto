package in.reqres.reto.controler;


import in.reqres.reto.exception.BaseException;
import in.reqres.reto.integration.impl.UsersImpl;
import in.reqres.reto.util.Constantes;
import in.reqres.reto.dto.UsersResponse;
import in.reqres.reto.integration.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.logging.Logger;

@RestController
@RequestMapping(Constantes.BASE_PATH)
public class RetoController {

    Logger logger = Logger.getLogger(String.valueOf(UsersImpl.class));

    @Autowired
    private UserService userService;

    @PostMapping(value = Constantes.PATH_USERS,
            produces = Constantes.APPLICATION_JSON)
    public UsersResponse getUsers() throws BaseException {
        String transaction = "method-getUsers-"+System.currentTimeMillis();
        return userService.getUsers(transaction);
    }
}
